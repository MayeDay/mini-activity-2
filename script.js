const adding = document.querySelector(".add").addEventListener("click", add);
const subing = document.querySelector(".sub").addEventListener("click", sub);
const multiply = document.querySelector(".multi").addEventListener("click", multi);
const dividing = document.querySelector(".divide").addEventListener("click", divide);
const moding = document.querySelector(".modulos").addEventListener("click", mod);
const clearing = document.querySelector(".clear").addEventListener("click", clear);
let input1 = 0;
let input2 = 0;
let sum = 0;


function compileNumbers(){
   input1 = Number(document.querySelector(".input1").value);
   input2 = Number(document.querySelector(".input2").value);
}

function showResult(newNumber){

  document.querySelector(".result").innerHTML = newNumber;
}

function add(){

  compileNumbers();

  sum = Number(input1 + input2);

  showResult(sum);
}

function sub(){
  compileNumbers();

  sum = input1 - input2;

  showResult(sum);
}

function multi(){
  compileNumbers();

  sum = input1 * input2;

  showResult(sum);
}

function divide(){
  compileNumbers();

  sum = input1 / input2;

  showResult(sum);
}

function mod(){
  compileNumbers();

  sum = input1 % input2;

  showResult(sum);
}

function clear(){
  input1 = document.querySelector(".input1").value = "";
  input2 = document.querySelector(".input2").value = "";
  document.querySelector(".result").innerHTML = "Operation Will Appear Here"
}